class Adder {
    constructor() { }
    add(arg1, arg2) {
        return arg1 + arg2;
    }

    subtract(arg1, arg2) {
        return arg1 - arg2;
    }

    multiply(arg1, arg2) {
        return arg1 * arg2;
    }

    addOrSubtract(arg1, arg2, isAdd) {
        if (isAdd) {
            return this.add(arg1, arg2);
        } else {
            return this.subtract(arg1, arg2);
        }
    }

    divide(arg1, arg2) {
        return arg1 / arg2;
    }
}

module.exports = Adder;